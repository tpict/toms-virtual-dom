# Tom's virtual DOM

React inspired virtual DOM for PicnicHealth. Honestly I struggled with
this. I probably made things much more complicated than they needed to be by
merging the state/props concepts - for this project I should have just had the
buttons replace the entire virtual tree and demonstrate that real DOM elements
only update as required.

My demo has a counter to show how often `document.createElement` is called for
each tag. You can see that it always increments for button (and div). This
is because props are given as a function that takes `updateProps` (a sort-of
`setState`). I wish I had more time to come up with a different way of
representing the virtual tree that still supported state changes.

## Setup

```
yarn
yarn serve
```

Demo is available on http://localhost:8080/
