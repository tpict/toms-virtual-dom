import { makePrimitive, renderCountNode } from "./Component";
import updater from "./updater";

import cat1 from "./cat1.jpg";
import cat2 from "./cat2.jpg";
import cat3 from "./cat3.jpg";
import cat4 from "./cat4.jpg";

const div = makePrimitive("div");
const ul = makePrimitive("ul");
const li = makePrimitive("li");
const button = makePrimitive("button");
const img = makePrimitive("img");

const cats = [cat1, cat2, cat3, cat4];

const tree = new div(null, () => [
  new div(
    updateProps => ({
      word: "I will change",
      updateWordBye: () =>
        updateProps({
          word: "goodbye"
        }),
      updateWordHi: () =>
        updateProps({
          word: "hello"
        })
    }),
    ({ word, updateWordBye, updateWordHi }) => [
      new ul({ word }, () => [
        new li(null, () => ["This is constant, and shouldn't re-render"]),
        new li({ word }, ({ word }) => [word])
      ]),
      new button({ onClick: updateWordBye }, () => [
        "Set second list item to 'goodbye'"
      ]),
      new button({ onClick: updateWordHi }, () => [
        "Set second list item to 'hello'"
      ])
    ]
  ),
  new div(
    updateProps => ({
      catIndex: 0,
      incrementCatIndex: () =>
        updateProps(({ catIndex }) => ({ catIndex: (catIndex + 1) % 4 }))
    }),
    ({ catIndex, incrementCatIndex }) => [
      new img(() => ({
        src: cats[catIndex],
        style: "height: 500px; display: block;"
      })),
      new button({ onClick: incrementCatIndex }, () => ["Update cat"])
    ]
  )
]);

const element = document.createElement("div");
element.appendChild(updater(null, tree));

const explanation = document.createElement("p");
explanation.append(
  document.createTextNode(
    "This counter keeps track of how many times each tag is rendered: "
  )
);
explanation.append(renderCountNode);

const sorry = document.createElement("p");
sorry.append(
  document.createTextNode(
    "The 'button' counter always increments :( see README.md"
  )
);

document.body.append(element);
document.body.append(explanation);
document.body.append(sorry);
