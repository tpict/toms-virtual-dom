import { isFunction } from "lodash";

import updater from "./updater";

export default function Component(props, children = () => []) {
  this.setParent = parent => (this.parent = parent);

  // This is bad because it causes unnecessary re-renders (see README.md)
  //
  // It's like a combination of props + state. I think there's a good reason
  // that the React team didn't do this :p
  this.updateProps = newProps => {
    // ========================================================================
    // this block lets us skip the update if the new props would have no effect
    let noChange = true;
    newProps = isFunction(newProps) ? newProps(this.props) : newProps;

    for (let [key, value] of Object.entries(
      isFunction(newProps) ? newProps(this.props) : newProps
    )) {
      if (this.props[key] !== value) {
        noChange = false;
        break;
      }
    }

    if (noChange) return;
    // ========================================================================

    const mergedProps = updateProps =>
      Object.assign({}, this._props(updateProps), newProps);

    updater(
      this.parent,
      new this.constructor(mergedProps, this._children),
      this
    );
  };

  this._props = isFunction(props) ? props : () => props || {};
  this._children = children;

  this.props = this._props(this.updateProps);
  this.children = children(this.props);

  // Convenience method so that subclasses don't have to worry about setting
  // el/parent
  this._render = parent => {
    this.setParent(parent);
    this.el = this.render();
    return this.el;
  };
}

// Convenience function for making base components (div, img, ul, li etc.)
// props beginning with "on" are assumed to be event handlers.
// updates the renderCount object on render.
export const makePrimitive = tag =>
  function(...args) {
    Component.call(this, ...args);

    this.render = () => {
      incrementRenderCount(tag);
      const el = document.createElement(tag);
      Object.entries(this.props).forEach(([attribute, value]) => {
        if (attribute.slice(0, 2) === "on") {
          el.addEventListener(attribute.slice(2).toLowerCase(), value);
        } else {
          el.setAttribute(attribute, value);
        }
      });
      return el;
    };
  };

// Node to display render counts for each tag
// I didn't implement this in my virtual DOM as I think it would require
// something like React context
export const renderCountNode = document.createTextNode("");
const renderCount = {};
const incrementRenderCount = tag => {
  renderCount[tag] ? renderCount[tag]++ : (renderCount[tag] = 1);
  renderCountNode.nodeValue = JSON.stringify(renderCount);
};
