import { isString } from "lodash";
import shallowEqualObjects from "shallow-equal/objects";

const updateChildren = (component, oldComponent) => {
  component.children.forEach((child, index) =>
    updater(component, child, oldComponent && oldComponent.children[index])
  );
};

const updater = (parent, component, oldComponent) => {
  let el;

  if (oldComponent) {
    // If an existing component has called updateProps

    if (
      !(component.props || oldComponent.props) ||
      shallowEqualObjects(oldComponent.props, component.props)
    ) {
      // If components have no props (e.g. text node) or have shallow
      // equivalency, we don't need to do any rendering

      if (isString(oldComponent)) {
        parent.el.append(component);
      } else {
        component.setParent(oldComponent.parent);
        component.el = oldComponent.el;
        component.children = oldComponent.children;
        parent.el.appendChild(component.el);
      }

      return;
    }

    // Otherwise, we need to render the new component
    component._render(parent);

    if (oldComponent.el.parentNode === parent.el) {
      // Insert the new element at the right position
      parent.el.insertBefore(component.el, oldComponent.el);
      parent.el.removeChild(oldComponent.el);
    } else {
      parent.el.appendChild(component.el);
    }

    updateChildren(component, oldComponent);
    return;
  }

  // If we're adding a new component
  if (isString(component)) {
    parent && parent.el.append(component);
  } else {
    el = component._render(parent);
    parent && parent.el.appendChild(el);
    updateChildren(component);
  }

  return el;
};

export default updater;
